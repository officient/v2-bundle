<?php

namespace Officient\V2\Exception;

/**
 * Class CouldNotLogException
 * @package Officient\V2\Exception
 */
class CouldNotLogException extends V2Exception
{

}