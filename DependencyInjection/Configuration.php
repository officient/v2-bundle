<?php

namespace Officient\V2\DependencyInjection;

use Symfony\Component\Config\Definition\Builder\TreeBuilder;
use Symfony\Component\Config\Definition\ConfigurationInterface;

/**
 * Class Configuration
 *
 * This class describes the configuration for this bundle
 *
 * @package Officient\V2\DependencyInjection
 */
class Configuration implements ConfigurationInterface
{
    /**
     * @inheritDoc
     */
    public function getConfigTreeBuilder()
    {
        $treeBuilder = new TreeBuilder('v2');

        $treeBuilder->getRootNode()
            ->children()
                ->scalarNode('host')->isRequired()->end()
                ->integerNode('port')->defaultNull()->end()
                ->scalarNode('prefix')->isRequired()->end()
                ->scalarNode('basic_auth_username')->defaultNull()->end()
                ->scalarNode('basic_auth_password')->defaultNull()->end()
            ->end();

        return $treeBuilder;
    }
}