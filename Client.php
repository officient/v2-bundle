<?php

namespace Officient\V2;

use Officient\V2\Exception\CouldNotConnectException;

/**
 * Class Client
 *
 * This class handles all communication with V2
 *
 * @package Officient\V2
 */
class Client implements ClientInterface
{
    /**
     * @var string
     */
    private $host;

    /**
     * @var int|null
     */
    private $port;

    /**
     * @var string|null
     */
    private $basicAuthUsername;

    /**
     * @var string|null
     */
    private $basicAuthPassword;

    /**
     * Client constructor.
     * @param string $host
     * @param int|null $port
     * @param string|null $basicAuthUsername
     * @param string|null $basicAuthPassword
     */
    public function __construct(string $host, ?int $port, ?string $basicAuthUsername, ?string $basicAuthPassword)
    {
        $this->host = $host;
        $this->port = $port;
        $this->basicAuthUsername = $basicAuthUsername;
        $this->basicAuthPassword = $basicAuthPassword;
    }

    /**
     * @inheritDoc
     */
    public function doRequest(string $query, $data = null, string $method = self::METHOD_GET): Response
    {
        $handle = curl_init();

        curl_setopt($handle, CURLOPT_URL, $this->getHost() . $query);

        if (!is_null($this->port)) {
            curl_setopt($handle, CURLOPT_PORT, $this->getPort());
        }
        curl_setopt($handle, CURLOPT_TIMEOUT, 30);
        curl_setopt($handle, CURLOPT_CUSTOMREQUEST, $method);
        curl_setopt($handle, CURLOPT_RETURNTRANSFER, 1);
        if ($method !== self::METHOD_GET) {
            if (!is_null($data)) {
                $postFields = json_encode($data);
                curl_setopt($handle, CURLOPT_POSTFIELDS, $postFields);
            }
        }

        if (is_null($this->basicAuthUsername) === false && is_null($this->basicAuthPassword) === false) {
            curl_setopt($handle, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
            curl_setopt($handle, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($handle, CURLOPT_USERPWD, "$this->basicAuthUsername:$this->basicAuthPassword");
        }

        $content = curl_exec($handle);
        $response_code = curl_getinfo($handle, CURLINFO_HTTP_CODE);
        curl_close($handle);

        if(!$content) {
            throw new CouldNotConnectException();
        }

        return new Response($response_code, $content);
    }

    private function getHost()
    {
        $host = $this->host;
        if(substr($host, -1) !== '/') {
            $host .= '/';
        }
        return $host;
    }

    private function getPort()
    {
        return $this->port;
    }
}