<?php

namespace Officient\V2\Exception;

/**
 * Class CouldNotConnectException
 * @package Officient\V2\Exception
 */
class CouldNotConnectException extends V2Exception
{

}