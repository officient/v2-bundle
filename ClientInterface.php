<?php

namespace Officient\V2;

use Officient\V2\Exception\FileDeliveryFailedException;
use Officient\V2\Exception\V2Exception;

/**
 * Interface ClientInterface
 * @package Officient\V2
 */
interface ClientInterface
{
    public const METHOD_GET = 'GET';
    public const METHOD_POST = 'POST';
    public const METHOD_PATCH = 'PATCH';
    public const METHOD_DELETE = 'DELETE';

    /**
     * @param string $query
     * @param null $data
     * @param string $method
     * @return Response
     * @throws V2Exception
     * @throws FileDeliveryFailedException
     */
    public function doRequest(string $query, $data = null, string $method = self::METHOD_GET): Response;
}