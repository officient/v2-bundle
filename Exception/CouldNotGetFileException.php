<?php

namespace Officient\V2\Exception;

/**
 * Class CouldNotGetFileException
 * @package Officient\V2\Exception
 */
class CouldNotGetFileException extends V2Exception
{

}