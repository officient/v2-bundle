<?php

namespace Officient\V2\Manager;

use Officient\V2\Content;
use Officient\V2\Exception\CouldNotGetFileException;
use Officient\V2\Exception\CouldNotGetLogException;
use Officient\V2\Exception\CouldNotLogException;
use Officient\V2\Exception\FileDeliveryFailedException;
use Officient\V2\Exception\V2Exception;
use Officient\V2\File;
use Officient\V2\LogEntry;

/**
 * Interface V2ManagerInterface
 * @package Officient\V2\Manager
 */
interface V2ManagerInterface
{
    const TYPE_ARCHIVE = 'A';
    const TYPE_DEBUG = 'D';
    const TYPE_ERROR = 'E';
    const TYPE_FILE_ACKNOWLEDGEMENT = 'F_A';
    const TYPE_FILE_FETCH = 'F_F';
    const TYPE_INFO = 'I';
    const TYPE_RECEIVE_N = 'R_N';
    const TYPE_TRANSPORT = 'T';
    const TYPE_TRANSPORT_ERROR = 'T_E';
    const TYPE_TRANSPORT_FILE_FOLDER = 'T_F';
    const TYPE_TRANSPORT_HOST = 'T_H';
    const TYPE_TRANSPORT_NAME = 'T_N';
    const TYPE_TRANSPORT_PROTOCOL = 'T_P';
    const TYPE_TRANSPORT_SIZE = 'T_S';
    const TYPE_WARNING = 'W';
    const TYPE_VERBOSE = 'V';

    /**
     * @param string $origFilename
     * @param string $fileContent
     * @param string $id
     * @param string|null $serviceTag
     * @param string|null $direction
     * @param string|null $prefix
     * @param string|null $externalReference
     * @param string|null $flowUuid
     * @param int|null $entrypointId
     * @param string|null $origFilepath
     * @return string - flowUuid
     * @throws V2Exception
     * @throws FileDeliveryFailedException
     */
    public function deliverFile(
        string  $origFilename,
        string  $fileContent,
        string  $id,
        ?string $serviceTag = null,
        ?string $direction = null,
        ?string $prefix = null,
        ?string $externalReference = null,
        ?string $flowUuid = null,
        ?int    $entrypointId = null,
        ?string $origFilepath = null
    ): string;

    /**
     * @param string $flowUuid
     * @return Content[]
     * @throws V2Exception
     * @throws CouldNotGetFileException
     */
    public function getFile(string $flowUuid): array;

    /**
     * @param string $flowUuid
     * @param string $message
     * @param string $type
     * @throws V2Exception
     * @throws CouldNotLogException
     */
    public function log(string $flowUuid, string $message, string $type, ?string $timestamp = null): void;

    /**
     * @param string $flowUuid
     * @return LogEntry[]
     * @throws V2Exception
     * @throws CouldNotGetLogException
     */
    public function getLog(string $flowUuid): array;

    /**
     * @param string $id
     * @param string $email For handling flow errors
     * @param int $configId
     * @param string|null $prefix
     * @throws V2Exception
     */
    public function createFlows(string $id, string $email, int $configId = 1, ?string $prefix = null): void;

    /**
     * @param string $data
     * @param string|null $mimeType
     * @param bool $base64Encode
     * @return array
     */
    public function validate(string $data, ?string $mimeType = null, bool $base64Encode = true, string $mappingFormat = 'XRechnung'): array;
}