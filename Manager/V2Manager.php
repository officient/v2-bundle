<?php

namespace Officient\V2\Manager;

use Officient\V2\Client;
use Officient\V2\ClientInterface;
use Officient\V2\Content;
use Officient\V2\Exception\CouldNotCreateFlowsException;
use Officient\V2\Exception\CouldNotGetFileException;
use Officient\V2\Exception\CouldNotGetLogException;
use Officient\V2\Exception\CouldNotLogException;
use Officient\V2\Exception\FileDeliveryFailedException;
use Officient\V2\Exception\V2Exception;
use Officient\V2\File;
use Officient\V2\Helper\FileHelper;
use Officient\V2\LogEntry;
use Ramsey\Uuid\Uuid;

/**
 * Class V2Manager
 * @package Officient\V2\Manager
 */
class V2Manager extends AbstractManager implements V2ManagerInterface
{
    /**
     * @var string
     */
    private $prefix;

    /**
     * @inheritDoc
     */
    public function __construct(ClientInterface $client, string $prefix)
    {
        parent::__construct($client);
        $this->prefix = strtoupper($prefix);
    }

    /**
     * @inheritDoc
     */
    public function deliverFile(
        string  $origFilename,
        string  $fileContent,
        string  $id,
        ?string $serviceTag = null,
        ?string $direction = null,
        ?string $prefix = null,
        ?string $externalReference = null,
        ?string $flowUuid = null,
        ?int    $entrypointId = null,
        ?string $origFilepath = null
    ): string
    {
        $oid = $this->getOid($id, $prefix);
        $base64 = base64_encode($fileContent);
        $md5 = md5($base64);
        $flowUuid = $flowUuid ?? Uuid::uuid4();

        $data = [
            'oid' => $oid,
            'orig_filename' => $origFilename,
            'flow_uuid' => $flowUuid,
            'md5' => $md5,
            'data' => $base64
        ];

        if($serviceTag) {
            $data['service_tag'] = $serviceTag;
        }

        if($direction) {
            $data['direction'] = $direction;
        }

        if($externalReference) {
            $data['external_reference'] = $externalReference;
        }

        if($entrypointId) {
            $data['entrypoint_id'] = $entrypointId;
        }

        if($origFilepath) {
            $data['orig_filepath'] = $origFilepath;
        }

        try {
            $response = $this->client->doRequest("inputhandler/receive.php", $data, Client::METHOD_POST);
            if(strpos(strtolower($response->getContent() ?? ''), 'data saved succesfully') === false) {
                throw new V2Exception('Failed delivering file. Return text did not contain \'Data saved succesfully\' (we do not trust http code on v2backend)');
            }

            return $flowUuid;
        } catch (V2Exception $exception) {
            if($exception->getCode() === 406) {
                throw new FileDeliveryFailedException("File $origFilename could not be delivered: ".$exception->getMessage(), null, $exception);
            } else {
                throw $exception;
            }
        }
    }

    /**
     * @inheritDoc
     */
    public function getFile(string $flowUuid): array
    {
        try {
            $response = $this->client->doRequest("service/archive.php", [
                'flow_uuid' => $flowUuid,
                'action' => 'GET'
            ], Client::METHOD_POST);

            $contents = array();

            // Temp V2 fix for at finde original
            $items = json_decode($response->getContent());
            foreach($items as $item) {
                $s = base64_decode($item->content);
                if(!json_decode($s)) {
                    $content = new Content();
                    $content->setBase64Data(base64_encode($s));
                    $content->setMimeType(FileHelper::guessMimeTypeFromContent($s));
                    $contents[] = $content;
                }
            }

            return $contents;
        } catch (V2Exception $exception) {
            if($exception->getCode() === 406) {
                throw new CouldNotGetFileException("Could not get file with flow uuid $flowUuid", null, $exception);
            } else {
                throw $exception;
            }
        }
    }

    /**
     * @inheritDoc
     */
    public function log(string $flowUuid, string $message, string $type, ?string $timestamp = null): void
    {
        try {
            $this->client->doRequest("service/syslog.php", [
                'flow_uuid' => $flowUuid,
                'logmessage' => $message,
                'logtype' => strtoupper($type),
                'action' => 'PUT',
                'logts' => $timestamp
            ], Client::METHOD_POST);
        } catch (V2Exception $exception) {
            throw new CouldNotLogException($exception->getMessage(), $exception->getCode(), $exception);
        }
    }

    /**
     * @inheritDoc
     */
    public function getLog(string $flowUuid): array
    {
        try {
            $response = $this->client->doRequest("service/syslog.php", [
                'flow_uuid' => $flowUuid,
                'action' => 'GET'
            ], Client::METHOD_POST);

            $result = array();
            $items = json_decode($response->getContent());
            foreach ($items as $item) {
                $logEntry = new LogEntry();
                $logEntry->setTimestamp(new \DateTime($item->ts));
                $logEntry->setType($item->type);
                $logEntry->setMessage($item->message);
                $result[] = $logEntry;
            }
            return $result;
        } catch (V2Exception $exception) {
            if($exception->getCode() === 406) {
                throw new CouldNotGetLogException("Could not get log for file with flow uuid $flowUuid", null, $exception);
            } else {
                throw $exception;
            }
        }
    }

    /**
     * @inheritDoc
     */
    public function createFlows(string $id, string $email, int $configId = 1, ?string $prefix = null): void
    {
        $oid = $this->getOid($id, $prefix);

        try {
            $this->client->doRequest("service/manage.php", [
                'action' => 'CREATE',
                'oid' => $oid,
                'config_id' => $configId,
                'flow_error_email' => $email
            ], Client::METHOD_POST);
        } catch (V2Exception $exception) {
            if($exception->getCode() === 406) {
                throw new CouldNotCreateFlowsException("V2 failed with code 406 and message: ".$exception->getMessage(), null, $exception);
            } else {
                throw $exception;
            }
        }
    }

    /**
     * @inheritDoc
     */
    public function validate(string $data, ?string $mimeType = null, bool $base64Encode = true, string $mappingFormat = 'XRechnung'): array
    {
        if($base64Encode) {
            $data = base64_encode($data);
        }
        try {
            $response = $this->client->doRequest("service/validate.php", [
                'data' => $data,
                'mime_type' => $mimeType,
                'mapping_format' => $mappingFormat
            ], Client::METHOD_POST);
            return json_decode($response->getContent(), true);
        } catch (V2Exception $exception) {
            if($exception->getCode() === 406) {
                throw new CouldNotCreateFlowsException("V2 failed with code 406 and message: ".$exception->getMessage(), null, $exception);
            } else {
                throw $exception;
            }
        }
    }


    /**
     * @param string $id
     * @param string|null $prefix
     * @return string
     */
    private function getOid(string $id, ?string $prefix = null): string
    {
        return ($prefix ?? $this->prefix).":".$id;
    }
}