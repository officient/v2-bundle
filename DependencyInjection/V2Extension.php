<?php

namespace Officient\V2\DependencyInjection;

use Officient\V2\Exception\V2Exception;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\DependencyInjection\Extension\Extension;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;

/**
 * Class V2Extension
 *
 * This class handles loading services and configurations
 *
 * @package Officient\V2\DependencyInjection
 */
class V2Extension extends Extension
{
    /**
     * @inheritDoc
     */
    public function load(array $configs, ContainerBuilder $container)
    {
        $configuration = new Configuration();
        $config = $this->processConfiguration($configuration, $configs);

        $loader = new YamlFileLoader(
            $container,
            new FileLocator(__DIR__.'/../Resources/config')
        );
        $loader->load('services.yaml');

        $clientDef = $container->getDefinition('Officient\V2\Client');
        $clientDef->replaceArgument(0, $config['host']);
        $clientDef->replaceArgument(1, $config['port']);
        $clientDef->replaceArgument(2, $config['basic_auth_username']);
        $clientDef->replaceArgument(3, $config['basic_auth_password']);

        if(empty($config['prefix'])) {
            throw new V2Exception("Prefix can not be empty!");
        }

        $v2ManDef = $container->getDefinition('Officient\V2\Manager\V2Manager');
        $v2ManDef->replaceArgument(1, $config['prefix']);
    }
}