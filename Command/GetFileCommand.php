<?php

namespace Officient\V2\Command;

use Officient\V2\Exception\CouldNotGetFileException;
use Officient\V2\Exception\V2Exception;
use Officient\V2\Manager\V2ManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class GetFileCommand extends Command
{
    /**
     * @var V2ManagerInterface
     */
    private $v2Manager;

    /**
     * @inheritDoc
     */
    public function __construct(V2ManagerInterface $v2Manager)
    {
        parent::__construct(null);
        $this->v2Manager = $v2Manager;
    }


    /**
     * @inheritDoc
     */
    protected function configure()
    {
        $this->setName('v2:getfile')
            ->setDescription('Gets a file from V2')
            ->addArgument('flow_uuid', InputArgument::REQUIRED, 'Flow UUID to get file from');
    }

    /**
     * @inheritDoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $flow_uuid = $input->getArgument('flow_uuid');

        try {
            $contents = $this->v2Manager->getFile($flow_uuid);
        } catch (CouldNotGetFileException $e) {
            $io->error($e->getMessage());
            return 1;
        } catch (V2Exception $e) {
            $io->error($e->getMessage());
            return 1;
        }

        $content_table = array();
        foreach ($contents as $key => $content) {
            $content_table[] = [
                $key,
                $flow_uuid,
                $content->getMimeType()
            ];
        }

        $io->table([
            'Index',
            'Flow UUID',
            'Mimetype'
        ], $content_table);

        $answer = $io->ask('Show content? [index/n]');
        if(is_numeric($answer)) {
            if(!isset($contents[$answer])) {
                $io->error('The specified index does not exist');
                return 1;
            }
            echo '=== CONTENT START ===\n'.base64_decode($contents[$answer]->getBase64Data())."\n=== CONTENT END ===\n";
        }

        return 0;
    }

}