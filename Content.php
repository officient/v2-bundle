<?php

namespace Officient\V2;

/**
 * Class Content
 * @package Officient\V2
 */
class Content implements \JsonSerializable
{
    /**
     * @var string|null
     */
    private $base64Data;

    /**
     * @var string|null
     */
    private $mimeType;

    /**
     * @inheritDoc
     */
    public function jsonSerialize(): mixed
    {
        return get_object_vars($this);
    }

    /**
     * @return string|null
     */
    public function getBase64Data(): ?string
    {
        return $this->base64Data;
    }

    /**
     * @param string|null $base64Data
     * @return Content
     */
    public function setBase64Data(?string $base64Data): Content
    {
        $this->base64Data = $base64Data;
        return $this;
    }

    /**
     * @return string|null
     */
    public function getData(): ?string
    {
        return !is_null($this->base64Data) ? base64_decode($this->base64Data) : null;
    }

    /**
     * @return string|null
     */
    public function getMimeType(): ?string
    {
        return $this->mimeType;
    }

    /**
     * @param string|null $mimeType
     * @return Content
     */
    public function setMimeType(?string $mimeType): Content
    {
        $this->mimeType = $mimeType;
        return $this;
    }
}