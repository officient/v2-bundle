<?php

namespace Officient\V2;

use DateTimeInterface;

/**
 * Class LogEntry
 * @package Officient\V2
 */
class LogEntry implements \JsonSerializable
{
    const TYPE_ARCHIVE = 'A';
    const TYPE_DEBUG = 'D';
    const TYPE_ERROR = 'E';
    const TYPE_FILE_ACKNOWLEDGEMENT = 'F_A';
    const TYPE_FILE_FETCH = 'F_F';
    const TYPE_INFO = 'I';
    const TYPE_RECEIVE_N = 'R_N';
    const TYPE_TRANSPORT = 'T';
    const TYPE_TRANSPORT_ERROR = 'T_E';
    const TYPE_TRANSPORT_FILE_FOLDER = 'T_F';
    const TYPE_TRANSPORT_HOST = 'T_H';
    const TYPE_TRANSPORT_NAME = 'T_N';
    const TYPE_TRANSPORT_PROTOCOL = 'T_P';
    const TYPE_TRANSPORT_SIZE = 'T_S';
    const TYPE_WARNING = 'W';
    const TYPE_VERBOSE = 'V';

    /**
     * @var string|null
     */
    private $message;

    /**
     * @var string|null
     */
    private $type;

    /**
     * @var DateTimeInterface
     */
    private $timestamp;

    /**
     * @inheritDoc
     */
    public function jsonSerialize(): mixed
    {
        return get_object_vars($this);
    }

    /**
     * @return string|null
     */
    public function getMessage(): ?string
    {
        return $this->message;
    }

    /**
     * @param string|null $message
     */
    public function setMessage(?string $message): void
    {
        $this->message = $message;
    }

    /**
     * @return string|null
     */
    public function getType(): ?string
    {
        return $this->type;
    }

    /**
     * @param string|null $type
     */
    public function setType(?string $type): void
    {
        $this->type = $type;
    }

    /**
     * @return DateTimeInterface
     */
    public function getTimestamp(): DateTimeInterface
    {
        return $this->timestamp;
    }

    /**
     * @param DateTimeInterface $timestamp
     */
    public function setTimestamp(DateTimeInterface $timestamp): void
    {
        $this->timestamp = $timestamp;
    }
}