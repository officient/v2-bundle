<?php

namespace Officient\V2\Exception;

/**
 * Class CouldNotCreateFlowsException
 * @package Officient\V2\Exception
 */
class CouldNotCreateFlowsException extends V2Exception
{

}