<?php

namespace Officient\V2;

use Officient\V2\Exception\V2Exception;

/**
 * Class Response
 *
 * This class handles the V2 response data
 *
 * @package Officient\V2
 */
class Response
{
    /**
     * @var int
     */
    private $code;

    /**
     * @var string
     */
    private $content;

    /**
     * Response constructor.
     * @param int $code
     * @param string $content
     * @throws V2Exception
     */
    public function __construct(int $code, string $content)
    {
        $this->content = $content;
        $this->code = $code;

        $this->processCode();
    }

    /**
     * @throws V2Exception
     */
    private function processCode()
    {
        if($this->code !== 200) {
            throw new V2Exception($this->getContent(), $this->getCode());
        }
    }

    /**
     * @return int
     */
    public function getCode(): int
    {
        return $this->code;
    }

    /**
     * @return string
     */
    public function getContent(): string
    {
        return $this->content;
    }
}