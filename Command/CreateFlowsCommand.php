<?php

namespace Officient\V2\Command;

use Officient\V2\Exception\CouldNotGetFileException;
use Officient\V2\Exception\V2Exception;
use Officient\V2\Manager\V2ManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

class CreateFlowsCommand extends Command
{
    /**
     * @var V2ManagerInterface
     */
    private $v2Manager;

    /**
     * @inheritDoc
     */
    public function __construct(V2ManagerInterface $v2Manager)
    {
        parent::__construct(null);
        $this->v2Manager = $v2Manager;
    }


    /**
     * @inheritDoc
     */
    protected function configure()
    {
        $this->setName('v2:createflows')
            ->setDescription('Create flows in v2')
            ->addArgument('company_id', InputArgument::REQUIRED, 'company_id')
            ->addArgument('email', InputArgument::REQUIRED, 'email')
            ->addArgument('config_id', InputArgument::REQUIRED, 'config_id');
    }

    /**
     * @inheritDoc
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $io = new SymfonyStyle($input, $output);
        $companyId = $input->getArgument('company_id');
        $email = $input->getArgument('email');
        $config_id = $input->getArgument('config_id');

        $this->v2Manager->createFlows($companyId, $email, $config_id);

        return 0;
    }

}