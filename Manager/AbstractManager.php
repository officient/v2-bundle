<?php

namespace Officient\V2\Manager;

use Officient\V2\ClientInterface;

/**
 * Class AbstractManager
 *
 * All managers should extend this class
 * A manager should handle the data flow between
 * the application and V2.
 *
 * @package Officient\V2\Manager
 */
abstract class AbstractManager
{
    /**
     * @var ClientInterface
     */
    protected $client;

    /**
     * Manager constructor.
     * @param ClientInterface $client
     */
    public function __construct(ClientInterface $client)
    {
        $this->client = $client;
    }
}