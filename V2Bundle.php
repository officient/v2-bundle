<?php

namespace Officient\V2;

use Symfony\Component\HttpKernel\Bundle\Bundle;

/**
 * Class V2Bundle
 *
 * This class just needs to be here, because of stuff
 * in symfony documentation...
 *
 * @package Officient\V2
 */
class V2Bundle extends Bundle
{

}