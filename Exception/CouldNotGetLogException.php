<?php

namespace Officient\V2\Exception;

/**
 * Class CouldNotGetLogException
 * @package Officient\V2\Exception
 */
class CouldNotGetLogException extends V2Exception
{

}