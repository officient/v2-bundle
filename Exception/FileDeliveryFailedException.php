<?php

namespace Officient\V2\Exception;

/**
 * Class FileDeliveryFailedException
 * @package Officient\V2\Exception
 */
class FileDeliveryFailedException extends V2Exception
{

}